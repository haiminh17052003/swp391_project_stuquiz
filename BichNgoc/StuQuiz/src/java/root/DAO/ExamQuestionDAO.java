/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package root.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import root.entities.main.ExamQuestion;
import root.jdbc.RowMapper;

/**
 *
 * @author admin
 */
public class ExamQuestionDAO implements RowMapper<ExamQuestion> {

    @Override
    public ExamQuestion mapRow(ResultSet rs) throws SQLException {
        return ExamQuestion.builder()
                .examQuestionId(rs.getLong("exam_question_id"))
                .quizId(rs.getLong("exam_id"))
                .questionId(rs.getLong("question_id"))
                .selectedAnswer(rs.getLong("selected_answer"))
                .build();
    }

    @Override
    public boolean addNew(ExamQuestion t) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ExamQuestion> getAll() throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ExamQuestion getById(String id) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean updateById(String id, ExamQuestion t) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean deleteById(String id) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
