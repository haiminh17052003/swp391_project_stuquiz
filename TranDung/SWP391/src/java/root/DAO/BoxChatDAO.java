/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package root.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import root.entities.main.BoxChat;
import root.entities.main.User;
import root.jdbc.RowMapper;
import root.jdbc.SQLServerConnection;

/**
 *
 * @author admin
 */
public class BoxChatDAO implements RowMapper<BoxChat> {

    @Override
    public BoxChat mapRow(ResultSet rs) throws SQLException {
        Long longDate = 0L;
        try {
            longDate = parseLong(rs.getString("send_time"));
        } catch (Exception e) {
            longDate = 0L;
        }
        return BoxChat.builder()
                .boxChatId(rs.getLong("boxchat_id"))
                .customerId(rs.getLong("customer_id"))
                .salesManId(rs.getLong("salesman_id"))
                .chatContent(rs.getString("chat_content"))
                .isCustomerSend(rs.getBoolean("is_customer_send"))
                .sendTime(new Date(longDate))
                .build();
    }

    @Override
    public boolean addNew(BoxChat t) throws SQLException, ClassNotFoundException {
        String sql = """
                     insert into [boxchat](
                     customer_id,
                     salesman_id,
                     chat_content,
                     is_customer_send,
                     send_time) values(?,?,?,?,?)
                     """;
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, t.getCustomerId());
            ps.setObject(2, t.getSalesManId());
            ps.setObject(3, t.getChatContent());
            ps.setObject(4, t.isCustomerSend());
            ps.setObject(5, t.getSendTime().getTime()+"");
            check = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return check > 0;
    }

    @Override
    public List<BoxChat> getAll() throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public BoxChat getById(String id) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean updateById(String id, BoxChat t) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean deleteById(String id) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<BoxChat> getByCustomerId(String customerId) {
        List<BoxChat> list = new ArrayList<>();
        String sql = """
              select * from [boxchat] where customer_id = ?
              """;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, parseLong(customerId));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(mapRow(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }

}
