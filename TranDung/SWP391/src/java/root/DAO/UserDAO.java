/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package root.DAO;

import jakarta.servlet.jsp.jstl.sql.Result;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import root.entities.main.User;
import root.jdbc.RowMapper;
import root.jdbc.SQLServerConnection;

/**
 *
 * @author admin
 */
public class UserDAO implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs) throws SQLException {
        Long longDate = parseLong(rs.getString("created_time"));
        Date createdTime = new Date(longDate);
        return User.builder()
                .userId(rs.getLong("user_id"))
                .userName(rs.getString("user_name"))
                .userEmail(rs.getString("user_email"))
                .userPassword(rs.getString("user_password"))
                .roleLevel(rs.getLong("role_level"))
                .userAvatar(rs.getString("user_avatar"))
                .userBank(rs.getString("user_bank"))
                .userBankCode(rs.getString("user_bank_code"))
                .createdTime(createdTime)
                .activeCode(rs.getString("active_code"))
                .isActive(rs.getBoolean("is_active"))
                .build();
    }

    @Override
    public boolean addNew(User t) throws SQLException, ClassNotFoundException {
        String sql = """
                     insert into [user](
                     [user_name],
                     user_email,
                     user_password,
                     role_level,
                     user_avatar,
                     user_bank,
                     user_bank_code,
                     created_time,
                     active_code,
                     is_active) values(?,?,?,?,?,?,?,?,?,?)
                     """;
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, t.getUserName());
            ps.setObject(2, t.getUserEmail());
            ps.setObject(3, t.getUserPassword());
            ps.setObject(4, t.getRoleLevel());
            ps.setObject(5, t.getUserAvatar());
            ps.setObject(6, t.getUserBank());
            ps.setObject(7, t.getUserBankCode());
            ps.setObject(8, t.getCreatedTime().getTime());
            ps.setObject(9, t.getActiveCode());
            ps.setObject(10, t.isActive());
            check = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return check > 0;
    }

    @Override
    public List<User> getAll() throws SQLException, ClassNotFoundException {
        List<User> list = new ArrayList<>();
        String sql = """
              select * from [user]
              """;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(mapRow(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public User getById(String id) throws SQLException, ClassNotFoundException {
        User user = new User();
        String sql = """
              select * from [user] where [user_id]=?
              """;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, parseLong(id));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = mapRow(rs);
            }
            if (user == null) {
                user = User.builder().userId(0L).build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public boolean updateById(String id, User t) throws SQLException, ClassNotFoundException {
        String sql = """
                     update [user]
                     set
                     [user_name] = ?,
                     user_password  = ?,
                     role_level = ?,
                     user_avatar = ?,
                     user_bank  = ?,
                     user_bank_code = ?,
                     created_time = ?,
                     active_code = ?,
                     is_active = ? where [user_id] = ?
                     """;
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, t.getUserName());
            ps.setObject(2, t.getUserPassword());
            ps.setObject(3, t.getRoleLevel());
            ps.setObject(4, t.getUserAvatar());
            ps.setObject(5, t.getUserBank());
            ps.setObject(6, t.getUserBankCode());
            ps.setObject(7, t.getCreatedTime().getTime());
            ps.setObject(8, t.getActiveCode());
            ps.setObject(9, t.isActive());
            ps.setObject(10, parseLong(id));
            check = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return check > 0;
    }

    public boolean setKeyByEmail(String email, String code) {
        String sql = """
                     update [user]
                     set  active_code = ? where user_email = ?
                     """;
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, code);
            ps.setObject(2, email);
            check = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return check > 0;
    }

    public boolean setKeyPassEmail(String email, String pass) {
        String sql = """
                     update [user]
                     set  user_password = ? where user_email = ?
                     """;
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, pass);
            ps.setObject(2, email);
            check = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return check > 0;
    }

    public String getKeyByEmail(String email)  {
        String sql = """
                  select active_code from [user] where user_email = ? 
                     """;
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String checkExistByEmail(String email)  {
        String sql = """
                  select user_email from [user] where user_email = ? 
                     """;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteById(String id) throws SQLException, ClassNotFoundException {
        String sql = """
                     delete from [user] where [user_id] = ?
                     """;
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, parseLong(id));
            check = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return check > 0;
    }

    public User getByEmail(String email) throws SQLException, ClassNotFoundException {
        User user = new User();
        user.setUserId(0L);
        String sql = """
              select * from [user] where [user_email]=?
              """;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = mapRow(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return user;
    }
}
