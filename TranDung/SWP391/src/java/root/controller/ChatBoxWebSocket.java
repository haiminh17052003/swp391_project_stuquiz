/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package root.controller;

import com.google.gson.Gson;
import jakarta.servlet.http.HttpSession;
import jakarta.websocket.OnClose;
import jakarta.websocket.OnError;
import jakarta.websocket.OnMessage;
import jakarta.websocket.OnOpen;
import jakarta.websocket.Session;
import jakarta.websocket.server.PathParam;
import jakarta.websocket.server.ServerEndpoint;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import root.DAO.BoxChatDAO;
import root.DAO.UserDAO;
import root.entities.main.BoxChat;
import root.entities.main.User;
import root.entities.sub.ResponseEntity;
import root.roleAndLevel.Kind;

/**
 *
 * @author admin
 */
@ServerEndpoint("/chat/{userId}")
public class ChatBoxWebSocket {

    public static Set<Session> users = Collections.synchronizedSet(new HashSet<>());

    @OnOpen
    public void onOpen(Session session, @PathParam("userId") String userId) {
        if (userId != null) {
            UserDAO userDAO = new UserDAO();
            try {
                User user = userDAO.getById(userId);
                if (user.getUserId() != 0L) {
                    session.getUserProperties().put("userAuthorization", user);
                }
            } catch (Exception e) {
            }
        }
        users.add(session);
    }

    @OnMessage
    public void onMessage(Session userSession, String message, @PathParam("userId") String userId) {
        Gson gson = new Gson();
        BoxChat boxChat = gson.fromJson(message, BoxChat.class);
        if (boxChat.getBoxChatId() == 0) {
            User user = (User) userSession.getUserProperties().get("userAuthorization");
            if (user == null) {
                if (userId.equals("0")) {
                    String json = gson.toJson(ResponseEntity.builder()
                            .kind(Kind.ERROR)
                            .data(new String("Please log in to get help!"))
                            .build());
                    try {
                        userSession.getBasicRemote().sendText(json);
                    } catch (Exception e) {
                    }
                } else {
                    UserDAO userDAO = new UserDAO();
                    User user1 = User.builder().userId(0L).build();

                    try {
                        System.out.println("hehehehehe");
                        user1 = userDAO.getById(userId);
                        System.out.println(user1);
                        if (user1.getUserId() != 0L) {
                            userSession.getUserProperties().put("userAuthorization", user1);
                        }
                    } catch (Exception e) {
                    }
                    try {

                        BoxChatDAO boxChatDAO = new BoxChatDAO();
                        List<BoxChat> listBoxChats = new ArrayList<>();
                        listBoxChats = boxChatDAO.getByCustomerId(userId);
                        String json = gson.toJson(ResponseEntity.builder()
                                .kind(Kind.MESSAGE)
                                .data(listBoxChats)
                                .build());
                        for (Session session : users) {
                            if (boxChat.getSalesManId() == ((User) session.getUserProperties().get("userAuthorization")).getUserId()
                                    || user1.getUserId() == ((User) session.getUserProperties().get("userAuthorization")).getUserId()) {
                                session.getBasicRemote().sendText(json);
                            }
                        }
                    } catch (Exception e) {
                    }
                }
            } else {
                try {
                    BoxChatDAO boxChatDAO = new BoxChatDAO();
                    boxChat.setSalesManId(5L);
                    boxChat.setCustomerId(user.getUserId());
                    boxChat.setSendTime(new Date());
                    List<BoxChat> listBoxChats = new ArrayList<>();

                    listBoxChats = boxChatDAO.getByCustomerId(user.getUserId() + "");
                    String json = gson.toJson(ResponseEntity.builder()
                            .kind(Kind.MESSAGE)
                            .data(listBoxChats)
                            .build());
                    for (Session session : users) {
                        if (boxChat.getSalesManId() == ((User) session.getUserProperties().get("userAuthorization")).getUserId()
                                || ((User) userSession.getUserProperties().get("userAuthorization")).getUserId()
                                == ((User) session.getUserProperties().get("userAuthorization")).getUserId()) {
                            session.getBasicRemote().sendText(json);
                        }
                    }
                } catch (Exception e) {
                }
            }
        } else {
            User user = (User) userSession.getUserProperties().get("userAuthorization");
            if (user == null) {
                if (userId.equals("0")) {
                    String json = gson.toJson(ResponseEntity.builder()
                            .kind(Kind.ERROR)
                            .data(new String("Please log in to get help!"))
                            .build());
                    try {
                        userSession.getBasicRemote().sendText(json);
                    } catch (Exception e) {
                    }
                } else {
                    UserDAO userDAO = new UserDAO();
                    User user1 = User.builder().userId(0L).build();

                    try {
                        user1 = userDAO.getById(userId);
                        System.out.println(userId);
                        if (user1.getUserId() != 0L) {
                            userSession.getUserProperties().put("userAuthorization", user1);
                        }
                    } catch (Exception e) {
                    }
                    try {

                        BoxChatDAO boxChatDAO = new BoxChatDAO();
                        boxChat.setSalesManId(5L);
                        boxChat.setCustomerId(boxChatDAO.parseLong(userId));
                        boxChat.setSendTime(new Date());
                        boxChatDAO.addNew(boxChat);
                        List<BoxChat> listBoxChats = new ArrayList<>();
                        listBoxChats = boxChatDAO.getByCustomerId(userId);
                        String json = gson.toJson(ResponseEntity.builder()
                                .kind(Kind.MESSAGE)
                                .data(listBoxChats)
                                .build());
                        for (Session session : users) {
                            if (boxChat.getSalesManId() == ((User) session.getUserProperties().get("userAuthorization")).getUserId()
                                    || user1.getUserId() == ((User) session.getUserProperties().get("userAuthorization")).getUserId()) {
                                session.getBasicRemote().sendText(json);
                            }
                        }
                    } catch (Exception e) {
                    }
                }
            } else {
                try {
                    BoxChatDAO boxChatDAO = new BoxChatDAO();
                    boxChat.setSalesManId(5L);
                    boxChat.setCustomerId(user.getUserId());
                    boxChat.setSendTime(new Date());
                    boxChatDAO.addNew(boxChat);

                    List<BoxChat> listBoxChats = new ArrayList<>();

                    listBoxChats = boxChatDAO.getByCustomerId(user.getUserId() + "");
                    String json = gson.toJson(ResponseEntity.builder()
                            .kind(Kind.MESSAGE)
                            .data(listBoxChats)
                            .build());
                    for (Session session : users) {
                        if (boxChat.getSalesManId() == ((User) session.getUserProperties().get("userAuthorization")).getUserId()
                                || ((User) userSession.getUserProperties().get("userAuthorization")).getUserId()
                                == ((User) session.getUserProperties().get("userAuthorization")).getUserId()) {
                            session.getBasicRemote().sendText(json);
                        }
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    @OnClose
    public void onClose(Session session) {
        users.remove(session);
    }

    @OnError
    public void handleError(Throwable t) {
        t.printStackTrace();
    }
}
