/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package root.entities.main;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author admin
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Subject {
    private Long subjectId;
    private Long ownerId;
    private Long courseId;
    private String subjectCode;
    private String subjectName;
    private String subjectDescription;
    private String subjectImage;
    private Double subjectPrice;
    private Double salePrice;
    private Date createdDate;
    private boolean isActive;
    private Course course;
    private User owner;

    public Subject(Long subjectId, Long ownerId, Long courseId, String subjectCode, String subjectName, String subjectDescription, String subjectImage, Double subjectPrice, Double salePrice, Date createdDate, boolean isActive) {
        this.subjectId = subjectId;
        this.ownerId = ownerId;
        this.courseId = courseId;
        this.subjectCode = subjectCode;
        this.subjectName = subjectName;
        this.subjectDescription = subjectDescription;
        this.subjectImage = subjectImage;
        this.subjectPrice = subjectPrice;
        this.salePrice = salePrice;
        this.createdDate = createdDate;
        this.isActive = isActive;
    }
    
}