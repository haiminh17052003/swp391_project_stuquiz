package root.entities.main;


import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import root.DAO.SubjectDAO;
import root.controller.UpdateSubject;
import java.util.Date;


@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User {

    private Long userId;
    private String userName;
    private String userEmail;
    private String userPassword;
    private Long roleLevel;
    private String userAvatar;
    private String userBank;
    private String userBankCode;
    private Date createdTime;
    private String activeCode;
    private boolean isActive;
}
