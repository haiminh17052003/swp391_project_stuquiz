-- Sample data for the user table
INSERT INTO [user] (user_name, user_email, user_password, role_level, user_avatar, user_bank, user_bank_code, created_time, active_code, is_active)
VALUES
('John Doe', 'john@example.com', 'password123', 1, 'avatar1.jpg', 'Bank A', '12345', '2024-05-31 10:00:00', 'ABCD', 1),
('Jane Smith', 'jane@example.com', 'password123', 2, 'avatar2.jpg', 'Bank B', '67890', '2024-05-31 10:00:00', 'EFGH', 1),
('Alice Johnson', 'alice@example.com', 'password123', 3, 'avatar3.jpg', 'Bank C', '54321', '2024-05-31 10:00:00', 'IJKL', 0),
('Chris Evans', 'chris@example.com', 'password123', 1, 'avatar4.jpg', 'Bank D', '11223', '2024-05-31 10:00:00', 'MNOP', 1),
('Scarlett Johansson', 'scarlett@example.com', 'password123', 2, 'avatar5.jpg', 'Bank E', '44556', '2024-05-31 10:00:00', 'QRST', 1),
('Robert Downey', 'robert@example.com', 'password123', 3, 'avatar6.jpg', 'Bank F', '77889', '2024-05-31 10:00:00', 'UVWX', 0),
('Mark Ruffalo', 'mark@example.com', 'password123', 1, 'avatar7.jpg', 'Bank G', '99001', '2024-05-31 10:00:00', 'YZAB', 1),
('Jeremy Renner', 'jeremy@example.com', 'password123', 2, 'avatar8.jpg', 'Bank H', '22334', '2024-05-31 10:00:00', 'CDEF', 1),
('Chris Hemsworth', 'hemsworth@example.com', 'password123', 3, 'avatar9.jpg', 'Bank I', '55667', '2024-05-31 10:00:00', 'GHJK', 0),
('Tom Holland', 'tom@example.com', 'password123', 1, 'avatar10.jpg', 'Bank J', '33445', '2024-05-31 10:00:00', 'LMNO', 1);
GO
-- Sample data for the course table
INSERT INTO course (course_name, course_code, is_active)
VALUES
('Computer Science', 'CS101', 1),
('Mathematics', 'MATH101', 1),
('Physics', 'PHYS101', 0),
('Biology', 'BIO101', 1),
('Chemistry', 'CHEM101', 1),
('English', 'ENG101', 0),
('History', 'HIST101', 1),
('Geography', 'GEO101', 1),
('Economics', 'ECON101', 0),
('Philosophy', 'PHIL101', 1);
GO
-- Sample data for the subject table
INSERT INTO [subject] (owner_id, course_id, subject_code, subject_name, subject_description, subject_image, subject_price, sale_price, created_date, is_active)
VALUES
(1, 1, 'CS101A', 'Introduction to Programming', 'Learn the basics of programming.', 'https://knowthecode.io/wp-content/uploads/2016/06/introduction-to-programming-300x158.png', 100.0, 80.0, '2024-01-30', 1),
(1, 2, 'MATH101A', 'Calculus I', 'Introduction to Calculus', 'https://wordsmithofbengal.files.wordpress.com/2021/08/calculus_score-sheet.png', 90.0, 70.0, '2024-02-27', 1),
(1, 3, 'PHYS101A', 'General Physics', 'Basic concepts of Physics', 'https://m.media-amazon.com/images/I/71U359cPuML._SY342_.jpg', 80.0, 60.0, '2024-03-26', 0),
(2, 4, 'BIO101A', 'Introduction to Biology', 'Basics of Biology', 'https://images.essentialresources.com.au/products/biology-basics-and-beyond---book-3_51101_l.png', 120.0, 50.0, '2024-04-25', 1),
(3, 5, 'CHEM101A', 'Organic Chemistry', 'Introduction to Organic Chemistry', 'https://m.media-amazon.com/images/I/61V8Pl3Yk7L._SY466_.jpg', 110.0, 30.0, '2023-05-24', 1),
(1, 6, 'ENG101A', 'English Literature', 'Overview of English Literature', 'https://m.media-amazon.com/images/I/61FwSZ+OefL._SY466_.jpg', 130.0, 50.0, '2023-06-23', 0),
(1, 7, 'HIST101A', 'World History', 'Study of World History', 'https://d3f1iyfxxz8i1e.cloudfront.net/courses/course_image/b85bd2f80cdb.jpg', 140.0, 20.0, '2023-07-22', 1),
(1, 8, 'GEO101A', 'Physical Geography', 'Basics of Physical Geography', 'https://images.tandf.co.uk/common/jackets/crclarge/978041555/9780415559300.jpg', 150.0, 10.0, '2023-08-21', 1),
(1, 9, 'ECON101A', 'Microeconomics', 'Introduction to Microeconomics', 'https://d1scgbpsz860hb.cloudfront.net/files/repository001/1691076435-introduction%20to%20microeconomics.png', 160.0, 20.0, '2023-09-20', 0),
(1, 10, 'PHIL101A', 'Philosophical Foundations', 'Introduction to Philosophy', 'https://media.wiley.com/product_data/coverImage300/23/07456166/0745616623.jpg', 170.0, 35.0, '2023-10-19', 1);
GO
-- Sample data for the lesson table
INSERT INTO lesson (lesson_name, subject_id, video_link, lesson_description, created_date, is_active)
VALUES
('Lesson 1', 1, 'video1.mp4', 'Introduction to Programming Lesson 1', '2024-05-31', 1),
('Lesson 2', 1, 'video2.mp4', 'Calculus I Lesson 1', '2024-05-31', 1),
('Lesson 3', 1, 'video3.mp4', 'General Physics Lesson 1', '2024-05-31', 0),
('Lesson 4', 1, 'video4.mp4', 'Biology Lesson 1', '2024-05-31', 1),
('Lesson 5', 1, 'video5.mp4', 'Chemistry Lesson 1', '2024-05-31', 1),
('Lesson 6', 1, 'video6.mp4', 'English Lesson 1', '2024-05-31', 0),
('Lesson 7', 1, 'video7.mp4', 'History Lesson 1', '2024-05-31', 1),
('Lesson 8', 1, 'video8.mp4', 'Geography Lesson 1', '2024-05-31', 1),
('Lesson 9', 1, 'video9.mp4', 'Economics Lesson 1', '2024-05-31', 0),
('Lesson 10', 1, 'video10.mp4', 'Philosophy Lesson 1', '2024-05-31', 1);
GO
-- Sample data for the question table
INSERT INTO question (subject_id, question_image, question_level, question_content, is_active)
VALUES 
(1, 'image1.jpg', 1, 'What is 2 + 2?', 1),
(1, 'image2.jpg', 2, 'What is the capital of France?', 1),
(2, 'image3.jpg', 3, 'Explain the theory of relativity.', 1),
(2, 'image4.jpg', 1, 'Is the Earth round?', 1),
(3, 'image5.jpg', 2, 'What is the boiling point of water?', 1),
(3, 'image6.jpg', 1, 'Is Python a programming language?', 1),
(4, 'image7.jpg', 3, 'Who wrote "To be, or not to be"?', 1),
(4, 'image8.jpg', 2, 'What is the chemical symbol for gold?', 1),
(5, 'image9.jpg', 1, 'What is the square root of 16?', 1),
(5, 'image10.jpg', 2, 'Does the sun rise in the east?', 1),
(6, 'image11.jpg', 2, 'Does the ?', 1);
GO
-- Sample data for the answer table
INSERT INTO answer (question_id, answer_content, is_true)
VALUES 
-- Question 1 (multiple choice)
(1, '3', 0),
(1, '4', 1),
(1, '5', 0),
(1, '6', 0),
-- Question 2 (multiple choice)
(2, 'Berlin', 0),
(2, 'Paris', 1),
(2, 'Rome', 0),
(2, 'Madrid', 0),
-- Question 3 (multiple choice)
(3, 'A scientific theory by Einstein', 1),
(3, 'A novel by Tolkien', 0),
(3, 'A painting by Da Vinci', 0),
(3, 'A type of music', 0),
-- Question 4 (true/false)
(4, 'True', 1),
(4, 'False', 0),
-- Question 5 (multiple choice)
(5, '90�C', 0),
(5, '100�C', 1),
(5, '80�C', 0),
(5, '120�C', 0),
-- Question 6 (true/false)
(6, 'True', 1),
(6, 'False', 0),
-- Question 7 (multiple choice)
(7, 'Shakespeare', 1),
(7, 'Newton', 0),
(7, 'Einstein', 0),
(7, 'Darwin', 0),
-- Question 8 (multiple choice)
(8, 'Au', 1),
(8, 'Ag', 0),
(8, 'Pb', 0),
(8, 'Fe', 0),
-- Question 9 (multiple choice)
(9, '3', 0),
(9, '4', 1),
(9, '5', 0),
(9, '6', 0),
-- Question 10 (true/false)
(10, 'True', 1),
(10, 'False', 0),
-- Question 11 (true/false)
(11, 'True', 1),
(11, 'False', 0);
GO
-- Sample data for the quiz table
INSERT INTO quiz (lesson_id, quiz_name, quiz_level, quiz_duration, pass_rate, quiz_type, quiz_description, e_question, m_question, h_question, is_active)
VALUES (1, 'Math Quiz', 1, 30, 70.0, 1, 'Basic math questions', 5, 3, 3, 1),
       (2, 'Geography Quiz', 2, 45, 75.0, 2, 'Questions about world capitals', 4, 4, 3, 1);  
GO
-- Sample data for the quiz_bank table
INSERT INTO quiz_bank (quiz_id, question_id)
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (1, 4),
       (1, 5),
       (1, 6),
       (1, 7),
       (1, 8),
       (1, 9),
       (1, 10),
	   (1, 11),
       (2, 1),
       (2, 2),
       (2, 3),
       (2, 4),
       (2, 5),
       (2, 6),
       (2, 7),
       (2, 8),
       (2, 9),
       (2, 10),
	   (2, 11);
GO
-- Sample data for the exam table
INSERT INTO exam (quiz_id, student_id, exam_rate, is_pass)
VALUES 
-- Student 1 taking Quiz 1 multiple times with calculated exam_rate
(1, 1, 5 * 10.00 / 11, 0), -- 4.54, Fail
(1, 1, 9 * 10.00 / 11, 1), -- 8.18, Pass
(1, 1, 10 * 10.00 / 11, 1), -- 9.09, Pass
(1, 1, 6 * 10.00 / 11, 1); -- 5.45, Pass
GO
select * from exam
-- Sample data for the exam_question table
INSERT INTO exam_question (exam_id, question_id, selected_answer)
VALUES 
-- First attempt by student 1 on Quiz 1 (4 correct answers out of 10)
(1, 1, 2), -- Correct
(1, 2, 5), -- Incorrect
(1, 3, 10), -- Incorrect
(1, 4, 14), -- Incorrect
(1, 5, 17), -- Incorrect
(1, 6, 20), -- Incorrect
(1, 7, 22), -- Incorrect
(1, 8, 25), -- Correct
(1, 9, 30), -- Correct
(1, 10, 33), -- Correct
(1, 11, 35), -- Correct

-- Second attempt by student 1 on Quiz 1 (8 correct answers out of 10)
(2, 1, 2), -- Correct
(2, 2, 6), -- Correct
(2, 3, 9), -- Correct
(2, 4, 13), -- Correct
(2, 5, 16), -- Correct
(2, 6, 19), -- Correct
(2, 7, 21), -- Correct
(2, 8, 25), -- Correct
(2, 9, 31), -- Incorrect
(2, 10, 34), -- Incorrect
(2, 11, 35), -- Correct
-- Third attempt by student 1 on Quiz 1 (9 correct answers out of 10)
(3, 1, 2), -- Correct
(3, 2, 6), -- Correct
(3, 3, 9), -- Correct
(3, 4, 13), -- Correct
(3, 5, 16), -- Correct
(3, 6, 19), -- Correct
(3, 7, 21), -- Correct
(3, 8, 25), -- Correct
(3, 9, 30), -- Correct
(3, 10, 34), -- Incorrect
(3, 11, 35), -- Correct

(4, 1, 2), -- Correct
(4, 2, 6), -- Correct
(4, 3, 9), -- Correct
(4, 4, 13), -- Correct
(4, 5, 16), -- Correct
(4, 6, 20), -- Correct
(4, 7, 22), -- Correct
(4, 8, 26), -- Correct
(4, 9, 31), -- Correct
(4, 10, 34), -- Incorrect
(4, 11, 35); -- Incorrect
GO
-- Sample data for the item table
INSERT INTO item (buyer_id, subject_id, item_price, qr_code, transaction_code, start_date, end_date)
VALUES
(1, 1, 100.0, 'QR1', 'TXN1', '2024-01-30', '2024-10-25'),
(1, 2, 90.0, 'QR2', 'TXN2', '2024-02-27', '2024-09-24'),
(1, 3, 80.0, 'QR3', 'TXN3', '2024-03-26', '2024-08-23'),
(2, 4, 70.0, 'QR4', 'TXN4', '2024-04-25', '2024-07-22'),
(3, 5, 60.0, 'QR5', 'TXN5', '2023-05-24', '2024-06-21'),
(1, 6, 50.0, 'QR6', 'TXN6', '2023-06-23', '2024-05-20'),
(1, 7, 40.0, 'QR7', 'TXN7', '2023-07-22', '2024-04-15'),
(1, 8, 30.0, 'QR8', 'TXN8', '2023-08-21', '2024-03-10'),
(1, 9, 20.0, 'QR9', 'TXN9', '2023-09-20', '2024-02-05'),
(1, 10, 10.0, 'QR10', 'TXN10', '2023-10-19', '2024-01-01');
GO
-- Sample data for the boxchat table
INSERT INTO boxchat (customer_id, salesman_id, chat_content, is_customer_send, send_time)
VALUES
(1, 2, 'Hello, I need help with my course.', 1, 10),
(2, 3, 'Sure, how can I assist you?', 0, 11),
(3, 1, 'Can you explain the last lesson?', 1, 12),
(4, 4, 'What is the deadline for this assignment?', 1, 13),
(5, 5, 'I need help with my quiz.', 0, 14),
(6, 6, 'How do I submit my project?', 1, 15),
(7, 7, 'Can I get a refund?', 0, 16),
(8, 8, 'How do I reset my password?', 1, 17),
(9, 9, 'Can I change my email address?', 0, 18),
(10, 10, 'How do I contact support?', 1, 19);
GO
-- Sample data for the comment table
INSERT INTO comment ([user_id], lesson_id, pre_comment, comment_content, comment_time, is_active)
VALUES
(1, 1, NULL, 'Great lesson!', '2024-05-31 10:00:00', 1),
(2, 2, NULL, 'Very helpful.', '2024-05-31 10:05:00', 1),
(3, 3, NULL, 'Needs more examples.', '2024-05-31 10:10:00', 0),
(4, 4, NULL, 'Well explained.', '2024-05-31 10:15:00', 1),
(5, 5, NULL, 'Too difficult.', '2024-05-31 10:20:00', 1),
(6, 6, NULL, 'Interesting content.', '2024-05-31 10:25:00', 0),
(7, 7, NULL, 'I learned a lot.', '2024-05-31 10:30:00', 1),
(8, 8, NULL, 'Not very clear.', '2024-05-31 10:35:00', 1),
(9, 9, NULL, 'Excellent!', '2024-05-31 10:40:00', 0),
(10, 10, NULL, 'More details needed.', '2024-05-31 10:45:00', 1);
GO
-- Sample data for the post table
INSERT INTO post (creator, post_content, post_image, hashTag, created_date)
VALUES
(1, 'Check out this new course!', 'post1.jpg', '#newcourse', '2024-05-31'),
(2, 'Loving the new lessons!', 'post2.jpg', '#education', '2024-05-31'),
(3, 'Just finished a quiz.', 'post3.jpg', '#quiz', '2024-05-31'),
(4, 'Can anyone help with this question?', 'post4.jpg', '#help', '2024-05-31'),
(5, 'This course is amazing!', 'post5.jpg', '#amazing', '2024-05-31'),
(6, 'I am struggling with this topic.', 'post6.jpg', '#struggle', '2024-05-31'),
(7, 'Completed my assignment!', 'post7.jpg', '#completed', '2024-05-31'),
(8, 'Looking for study partners.', 'post8.jpg', '#study', '2024-05-31'),
(9, 'Does anyone have notes for this?', 'post9.jpg', '#notes', '2024-05-31'),
(10, 'I need feedback on my project.', 'post10.jpg', '#feedback', '2024-05-31');
GO
-- Sample data for the vote table
INSERT INTO vote (maker_id, lesson_id, star)
VALUES
(1, 1, 5),
(2, 2, 4),
(3, 3, 3),
(4, 4, 5),
(5, 5, 4),
(6, 6, 3),
(7, 7, 5),
(8, 8, 4),
(9, 9, 3),
(10, 10, 5);
GO
-- Sample data for the subject_statistic table
INSERT INTO subject_statistic (subject_id, revenue, purchases, views)
VALUES
(1, 5000.0, 100, 1000),
(2, 4500.0, 90, 900),
(3, 4000.0, 80, 800),
(4, 3500.0, 70, 700),
(5, 3000.0, 60, 600),
(6, 2500.0, 50, 500),
(7, 2000.0, 40, 400),
(8, 1500.0, 30, 300),
(9, 1000.0, 20, 200),
(10, 500.0, 10, 100);