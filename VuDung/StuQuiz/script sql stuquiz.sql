﻿-- Total sql used in project Stuquiz - Tasks of Dũng Vũ - HE172447	

-- Table user

-- Get user by ID x3
SELECT * FROM [user] WHERE [user_id] = ?

-- Update user by ID x2
update [user]
             set
             [user_name] = ?,
             user_password  = ?,
             role_level = ?,
             user_avatar = ?,
             user_bank  = ?,
             user_bank_code = ?,
             created_time = ?,
             active_code = ?,
             is_active = ? where [user_id] = ?

-- Table courses
-- Get all courses
SELECT * FROM [course]

-- Add new course
INSERT INTO [course] (course_name, course_code, is_active) VALUES (?, ?, ?)

-- Update course
UPDATE [course] SET course_name = ?, course_code = ?, is_active = ? WHERE course_id = ?

-- Delete course
BEGIN TRANSACTION;
                     
                 DELETE FROM item WHERE subject_id IN (SELECT subject_id FROM [subject] WHERE course_id = ?);
                 DELETE FROM comment WHERE lesson_id IN (SELECT lesson_id FROM lesson WHERE subject_id IN (SELECT subject_id FROM [subject] WHERE course_id = ?));
                 DELETE FROM vote WHERE lesson_id IN (SELECT lesson_id FROM lesson WHERE subject_id IN (SELECT subject_id FROM [subject] WHERE course_id = ?));
                 DELETE FROM subject_statistic WHERE subject_id IN (SELECT subject_id FROM [subject] WHERE course_id = ?);
                 DELETE FROM exam_question WHERE question_id IN (SELECT question_id FROM question WHERE subject_id IN (SELECT subject_id FROM [subject] WHERE course_id = ?));
                 DELETE FROM quiz_bank WHERE question_id IN (SELECT question_id FROM question WHERE subject_id IN (SELECT subject_id FROM [subject] WHERE course_id = ?));
                 DELETE FROM answer WHERE question_id IN (SELECT question_id FROM question WHERE subject_id IN (SELECT subject_id FROM [subject] WHERE course_id = ?));
                 DELETE FROM question WHERE subject_id IN (SELECT subject_id FROM [subject] WHERE course_id = ?);
                 DELETE FROM exam WHERE quiz_id IN (SELECT quiz_id FROM quiz WHERE lesson_id IN (SELECT lesson_id FROM lesson WHERE subject_id IN (SELECT subject_id FROM [subject] WHERE course_id = ?)));
                 DELETE FROM quiz WHERE lesson_id IN (SELECT lesson_id FROM lesson WHERE subject_id IN (SELECT subject_id FROM [subject] WHERE course_id = ?));
                 DELETE FROM lesson WHERE subject_id IN (SELECT subject_id FROM [subject] WHERE course_id = ?);
                 DELETE FROM [subject] WHERE course_id = ?;
                 DELETE FROM course WHERE course_id = ?;
                     
                 COMMIT TRANSACTION;
-- Table subject

-- Get enrolled subject by userId x2
SELECT s.*
FROM [subject] s
JOIN item i ON s.subject_id = i.subject_id
WHERE i.buyer_id = ?

-- Get enroll subject and status by subject_Id
SELECT s.* 
      FROM 
      subject s 
      WHERE 
      s.subject_id = ?;

-- Table question
-- Get question by id
select * from [question] where [question_id]=?

-- Table answer
-- Get list answer by question_Id
select * from [answer] where question_id = ?

-- Table quiz
-- Get quiz by Id x2
select * from quiz where quiz_id=?

-- Table exam

-- Get exam by id 
select * from exam where exam_id = ?

-- Get list exam result by student_id and quiz_id
SELECT 
      e.*,
      (SELECT COUNT(*) 
      FROM exam_question eq 
      JOIN answer a ON eq.selected_answer = a.answer_id 
      WHERE eq.exam_id = e.exam_id AND a.is_true = 1) AS total_correct_answers
      FROM 
      exam e
      WHERE 
      e.student_id = ? AND e.quiz_id = ?;

-- Table exam_question
-- Get list exam question by exam_Id
select * from exam_question where exam_id = ?;

-- Get exam result by exam_id and student_id and quiz_id
SELECT 
      e.*,
      (SELECT COUNT(*) 
      FROM exam_question eq 
      JOIN answer a ON eq.selected_answer = a.answer_id 
      WHERE eq.exam_id = e.exam_id AND a.is_true = 1) AS total_correct_answers
      FROM 
      exam e
      WHERE 
      e.exam_id = ? AND e.student_id = ? AND e.quiz_id = ?;
-- Table item

-- Get item by buyer_Id and subject_Id x2
select * from item where buyer_id = ? and subject_id = ?


ALTER DATABASE stuquiz SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
DROP DATABASE stuquiz;