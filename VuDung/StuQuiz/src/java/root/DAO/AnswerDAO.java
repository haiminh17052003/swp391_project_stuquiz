/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package root.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import root.entities.main.Answer;
import root.jdbc.RowMapper;
import root.jdbc.SQLServerConnection;

/**
 *
 * @author vtdle
 */
public class AnswerDAO implements RowMapper<Answer> {

    @Override
    public Answer mapRow(ResultSet rs) throws SQLException {
        return Answer.builder()
                .answerId(rs.getLong("answer_id"))
                .questionId(rs.getLong("question_id"))
                .answerContent(rs.getString("answer_content"))
                .isTrue(rs.getBoolean("is_true"))
                .build();
    }

    @Override
    public boolean addNew(Answer t) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Answer> getAll() throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Answer getById(String id) throws SQLException, ClassNotFoundException {
        Answer answer = null;
        String sql = """
              select * from [answer] where [answer_id]=?
              """;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, parseLong(id));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                answer = mapRow(rs);
            }
            if (answer == null) {
                answer = Answer.builder().answerId(0L).isTrue(false).build();
            }
        } catch (SQLException | ClassNotFoundException e) {
        }
        return answer;
    }

    @Override
    public boolean updateById(String id, Answer t) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean deleteById(String id) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<Answer> getListAnswerByQuestionId(String questionId) throws SQLException, ClassNotFoundException {
        List<Answer> list = new ArrayList<>();
        String sql = """
                     select * from [answer] where question_id = ?
                     """;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, parseLong(questionId));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(mapRow(rs));
            }
        } catch (SQLException | ClassNotFoundException e) {
        }
        return list;
    }

}
