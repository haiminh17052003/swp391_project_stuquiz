/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package root.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import root.entities.main.BoxChat;
import root.jdbc.RowMapper;

/**
 *
 * @author vtdle
 */
public class BoxChatDAO implements RowMapper<BoxChat> {

    @Override
    public BoxChat mapRow(ResultSet rs) throws SQLException {
        return BoxChat.builder()
                .boxChatId(rs.getLong("boxchat_id"))
                .customerId(rs.getLong("customer_id"))
                .salesManId(rs.getLong("salesman_id"))
                .chatContent(rs.getString("chat_content"))
                .isCustomerSend(rs.getBoolean("is_customer_send"))
                .sendTime(rs.getDate("send_time"))
                .build();
    }

    @Override
    public boolean addNew(BoxChat t) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<BoxChat> getAll() throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public BoxChat getById(String id) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean updateById(String id, BoxChat t) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean deleteById(String id) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
