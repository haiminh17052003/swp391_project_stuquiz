///*
// * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
// * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
// */
//package root.DAO;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//import root.entities.sub.EnrolledSubject;
//import root.jdbc.RowMapper;
//import root.jdbc.SQLServerConnection;
//
///**
// *
// * @author vtdle
// */
//public class EnrolledSubjectDAO implements RowMapper<EnrolledSubject> {
//
//    @Override
//    public EnrolledSubject mapRow(ResultSet rs) throws SQLException {
//        return EnrolledSubject.builder()
//                .subjectId(rs.getLong("subject_id"))
//                .subjectCode(rs.getString("subject_code"))
//                .subjectName(rs.getString("subject_name"))
//                .subjectDescription(rs.getString("subject_description"))
//                .subjectImage(rs.getString("subject_image"))
//                .subjectPrice(rs.getDouble("subject_price"))
//                .buyerId(rs.getLong("buyer_id"))
//                .startDate(rs.getDate("start_date"))
//                .endDate(rs.getDate("end_date"))
//                .build();
//    }
//
//    @Override
//    public boolean addNew(EnrolledSubject t) throws SQLException, ClassNotFoundException {
//        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
//    }
//
//    @Override
//    public List<EnrolledSubject> getAll() throws SQLException, ClassNotFoundException {
//        List<EnrolledSubject> list = new ArrayList<>();
//        String sql = "SELECT \n"
//                + "    u.user_id,\n"
//                + "    u.user_name,\n"
//                + "    u.user_email,\n"
//                + "    u.user_password,\n"
//                + "    u.role_level,\n"
//                + "    u.user_avatar,\n"
//                + "    u.user_bank,\n"
//                + "    u.user_bank_code,\n"
//                + "    u.created_time,\n"
//                + "    u.active_code,\n"
//                + "    u.is_active,\n"
//                + "    s.subject_id,\n"
//                + "    s.owner_id,\n"
//                + "    s.course_id,\n"
//                + "    s.subject_code,\n"
//                + "    s.subject_name,\n"
//                + "    s.subject_description,\n"
//                + "    s.subject_image,\n"
//                + "    s.subject_price,\n"
//                + "    s.sale_price,\n"
//                + "    s.created_date,\n"
//                + "    s.is_active,\n"
//                + "    i.item_id,\n"
//                + "    i.buyer_id,\n"
//                + "    i.subject_id,\n"
//                + "    i.item_price,\n"
//                + "    i.qr_code,\n"
//                + "    i.transaction_code,\n"
//                + "    i.start_date,\n"
//                + "    i.end_date\n"
//                + "FROM \n"
//                + "    [user] u\n"
//                + "INNER JOIN \n"
//                + "    [subject] s ON u.user_id = s.owner_id\n"
//                + "INNER JOIN \n"
//                + "    item i ON u.user_id = i.buyer_id";
//        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                list.add(mapRow(rs));
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        return list;
//    }
//
//    @Override
//    public EnrolledSubject getById(String id) throws SQLException, ClassNotFoundException {
//        EnrolledSubject enrolledSubject = new EnrolledSubject();
//        String sql = "SELECT s.*\n"
//                + "FROM [subject] s\n"
//                + "JOIN item i ON s.subject_id = i.subject_id\n"
//                + "WHERE i.buyer_id = ?";
//        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
//            ps.setString(1, id);
//            ResultSet rs = ps.executeQuery();
//            if (rs.next()) {
//                enrolledSubject = mapRow(rs);
//            }
//        } catch (SQLException | ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        return enrolledSubject;
//    }
//
//    public static void main(String[] args) throws SQLException, ClassNotFoundException {
//        EnrolledSubjectDAO dao = new EnrolledSubjectDAO();
//        EnrolledSubject s = dao.getById("1");
//        System.out.println(s);
//    }
//
//    @Override
//    public boolean updateById(String id, EnrolledSubject t) throws SQLException, ClassNotFoundException {
//        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
//    }
//
//    @Override
//    public boolean deleteById(String id) throws SQLException, ClassNotFoundException {
//        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
//    }
//
//    public List<EnrolledSubject> searchByCodeOrName(String searchTerm) throws SQLException, ClassNotFoundException {
//        List<EnrolledSubject> list = new ArrayList<>();
//        String sql = "SELECT s.subject_id, s.subject_code, s.subject_name, s.subject_description, s.subject_image, s.subject_price, "
//                + "i.item_id, i.buyer_id, i.start_date, i.end_date "
//                + "FROM item i "
//                + "JOIN subject s ON i.subject_id = s.subject_id "
//                + "WHERE s.subject_code LIKE ? OR s.subject_name LIKE ?";
//        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
//            String searchPattern = "%" + searchTerm + "%";
//            ps.setString(1, searchPattern);
//            ps.setString(2, searchPattern);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                list.add(mapRow(rs));
//            }
//        } catch (SQLException | ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        return list;
//    }
//}
