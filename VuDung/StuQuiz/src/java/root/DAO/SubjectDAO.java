/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package root.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import root.entities.main.Subject;
import root.entities.sub.SubjectEnrolledAndStatus;
import root.jdbc.RowMapper;
import root.jdbc.SQLServerConnection;

/**
 *
 * @author vtdle
 */
public class SubjectDAO implements RowMapper<Subject> {

    @Override
    public Subject mapRow(ResultSet rs) throws SQLException {
        return Subject.builder()
                .subjectId(rs.getLong("subject_id"))
                .ownerId(rs.getLong("owner_id"))
                .courseId(rs.getLong("course_id"))
                .subjectCode(rs.getString("subject_code"))
                .subjectName(rs.getString("subject_name"))
                .subjectDescription(rs.getString("subject_description"))
                .subjectImage(rs.getString("subject_image"))
                .subjectPrice(rs.getDouble("subject_price"))
                .salePrice(rs.getDouble("sale_price"))
                .createdDate(rs.getDate("created_date"))
                .isActive(rs.getBoolean("is_active"))
                .build();
    }

    @Override
    public boolean addNew(Subject t) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Subject> getAll() throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Subject getById(String id) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean updateById(String id, Subject t) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean deleteById(String id) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<Subject> getEnrolledSubjectByUserId(String userId) throws SQLException, ClassNotFoundException {
        List<Subject> list = new ArrayList<>();
        String sql = """
                     SELECT s.*
                     FROM [subject] s
                     JOIN item i ON s.subject_id = i.subject_id
                     WHERE i.buyer_id = ?
                     """;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setObject(1, parseLong(userId));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(mapRow(rs));
            }
        } catch (SQLException | ClassNotFoundException e) {
        }

        return list;
    }

    public List<Subject> searchSubjects(String searchString) throws SQLException, ClassNotFoundException {
        List<Subject> list = new ArrayList<>();
        String sql = """
                     SELECT s.*
                     FROM [subject] s
                     JOIN item i ON s.subject_id = i.subject_id
                     WHERE s.subject_code LIKE ? OR s.subject_name LIKE ?
                     """;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, "%" + searchString + "%");
            ps.setString(2, "%" + searchString + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(mapRow(rs));
            }
        } catch (SQLException | ClassNotFoundException e) {
        }

        return list;
    }

    public List<SubjectEnrolledAndStatus> getEnrollSubjectAndStatusBySubjectId(String subjectId) throws SQLException, ClassNotFoundException {
        List<SubjectEnrolledAndStatus> list = new ArrayList<>();
        String sql = """
                     SELECT 
                         s.* 
                     FROM 
                         subject s 
                     WHERE 
                         s.subject_id = ?;
                     """;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setObject(1, Long.valueOf(subjectId));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Subject subject = mapRow(rs);
                Double salePrice = rs.getDouble("sale_price");

                SubjectEnrolledAndStatus subjectEnrolledAndStatus = SubjectEnrolledAndStatus.builder()
                        .subject(subject)
                        .salePrice(salePrice)
                        .build();
                list.add(subjectEnrolledAndStatus);
            }
        } catch (SQLException e) {
        }
        return list;
    }

}
