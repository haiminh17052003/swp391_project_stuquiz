package root.DAO;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import root.entities.main.User;
import root.jdbc.RowMapper;
import root.jdbc.SQLServerConnection;

/**
 *
 * @author vtdle
 */
public class UserDAO implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs) throws SQLException {
        Long longDate = parseLong(rs.getString("created_time"));
        Date createdTime = new Date(longDate);
        return User.builder()
                .userId(rs.getLong("user_id"))
                .userName(rs.getString("user_name"))
                .userEmail(rs.getString("user_email"))
                .userPassword(rs.getString("user_password"))
                .roleLevel(rs.getLong("role_level"))
                .userAvatar(rs.getString("user_avatar"))
                .userBank(rs.getString("user_bank"))
                .userBankCode(rs.getString("user_bank_code"))
                .createdTime(createdTime)
                .activeCode(rs.getString("active_code"))
                .isActive(rs.getBoolean("is_active"))
                .build();
    }

    @Override
    public boolean addNew(User t) throws SQLException, ClassNotFoundException {
        String sql = """
                     insert into [user](
                     [user_name],
                     user_email,
                     user_password,
                     role_level,
                     user_avatar,
                     user_bank,
                     user_bank_code,
                     created_time,
                     active_code,
                     is_active) values(?,?,?,?,?,?,?,?,?,?)
                     """;
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, t.getUserName());
            ps.setObject(2, t.getUserEmail());
            ps.setObject(3, t.getUserPassword());
            ps.setObject(4, t.getRoleLevel());
            ps.setObject(5, t.getUserAvatar());
            ps.setObject(6, t.getUserBank());
            ps.setObject(7, t.getUserBankCode());
            ps.setObject(8, t.getCreatedTime().getTime());
            ps.setObject(9, t.getActiveCode());
            ps.setObject(10, t.isActive());
            check = ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
        }
        return check > 0;
    }

    @Override
    public List<User> getAll() throws SQLException, ClassNotFoundException {
        List<User> list = new ArrayList<>();
        String sql = """
              select * from [user]
              """;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(mapRow(rs));
            }
        } catch (SQLException | ClassNotFoundException e) {
        }
        return list;
    }

    @Override
    public User getById(String id) throws SQLException, ClassNotFoundException {
        User user = null;
        String sql = "SELECT * FROM [user] WHERE [user_id] = ?";
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setObject(1, parseLong(id));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = mapRow(rs);
            } else {
                user = User.builder()
                        .userId(0L)
                        .build();
            }
        } catch (SQLException | ClassNotFoundException e) {
            user = User.builder()
                    .userId(0L)
                    .build();
        }

        return user;
    }

    @Override
    public boolean updateById(String id, User t) throws SQLException, ClassNotFoundException {
        String sql = """
                     update [user]
                     set
                     [user_name] = ?,
                     user_password  = ?,
                     role_level = ?,
                     user_avatar = ?,
                     user_bank  = ?,
                     user_bank_code = ?,
                     created_time = ?,
                     active_code = ?,
                     is_active = ? where [user_id] = ?
                     """;
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, t.getUserName());
            ps.setObject(2, t.getUserPassword());
            ps.setObject(3, t.getRoleLevel());
            ps.setObject(4, t.getUserAvatar());
            ps.setObject(5, t.getUserBank());
            ps.setObject(6, t.getUserBankCode());
            ps.setObject(7, t.getCreatedTime().getTime());
            ps.setObject(8, t.getActiveCode());
            ps.setObject(9, t.isActive());
            ps.setObject(10, t.getUserId());
            check = ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
        }
        return check > 0;
    }

    @Override
    public boolean deleteById(String id) throws SQLException, ClassNotFoundException {
        String sql = """
                     delete from [user] where [user_id] = ?
                     """;
        int check = 0;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, parseLong(id));
            check = ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
        }
        return check > 0;
    }

}
