create database stuquiz;
GO
use stuquiz;
GO
create table [user](
[user_id] int primary key identity(1,1),
[user_name] nvarchar(50) not null,
user_email varchar(50) not null unique,
user_password varchar(50) not null,
role_level int,
user_avatar varchar(300),
user_bank varchar(30),
user_bank_code varchar(100),
created_time varchar(500) not null,
active_code varchar(10) not null,
is_active bit default(0) not null
);
GO
create table course(
course_id int primary key identity(1,1),
course_name nvarchar(100) not null,
course_code varchar(20) not null unique,
is_active bit default(0) not null
);
GO
create table [subject](
subject_id int primary key identity(1,1),
owner_id int references [user]([user_id]) not null,
course_id int references course(course_id) not null,
subject_code varchar(20) not null unique,
subject_name nvarchar(100) not null,
subject_description nvarchar(4000),
subject_image varchar(500),
subject_price float not null,
sale_price float not null,
created_date date not null,
is_active bit default(0) not null
);
GO
create table lesson(
lesson_id int primary key identity(1,1),
lesson_name nvarchar(100),
subject_id int references [subject](subject_id),
video_link varchar(500),
lesson_description nvarchar(4000),
created_date date not null,
is_active bit default(0) not null
);
GO
create table question(
question_id int primary key identity(1,1),
subject_id int references [subject](subject_id),
question_image varchar(500),
question_level int not null,
question_content varchar(4000),
is_active bit default(0) not null
);
GO
create table answer(
answer_id int primary key identity(1,1),
question_id int references question(question_id) not null,
answer_content nvarchar(4000) not null,
is_true bit default(0) not null
);
GO
create table quiz(
quiz_id int primary key identity(1,1),
lesson_id int references lesson(lesson_id) not null,
quiz_name nvarchar(100) not null,
quiz_level int not null,
quiz_duration int not null,
pass_rate float not null,
quiz_type int not null,
quiz_description nvarchar(200),
e_question int not null,
m_question int not null,
h_question int not null,
is_active bit default(0) not null
);
GO
create table quiz_bank(
quiz_bank_id int primary key identity(1,1),
quiz_id int references quiz(quiz_id),
question_id int references question(question_id)
);
GO
create table exam(
exam_id int primary key identity(1,1),
quiz_id int references quiz(quiz_id),
student_id int references [user]([user_id]),
exam_rate float,
is_pass bit default(0)
);
GO
create table exam_question(
exam_question_id int primary key identity(1,1),
exam_id int references exam(exam_id),
question_id int references question(question_id),
selected_answer int references answer(answer_id)
);
GO
create table item(
item_id int primary key identity(1,1),
buyer_id int references [user]([user_id]) not null,
subject_id int references [subject](subject_id) not null,
item_price float not null,
qr_code varchar(500),
transaction_code varchar(500),
[start_date] date not null,
end_date date not null
);
GO
create table boxchat(
boxchat_id int primary key identity(1,1),
customer_id int references [user]([user_id]) not null,
salesman_id int references [user]([user_id]) not null,
chat_content nvarchar(500) not null,
is_customer_send bit not null default(0),
send_time int not null
);
GO
create table comment(
comment_id int primary key identity(1,1),
[user_id] int references [user]([user_id]) not null,
lesson_id int references lesson(lesson_id) not null,
pre_comment int,
comment_content nvarchar(1500),
comment_time varchar(500),
is_active bit default(0)
);
GO
create table post(
post_id int primary key identity(1,1),
creator int references [user]([user_id]) not null,
post_content nvarchar(4000),
post_image varchar(500),
hashTag varchar(500),
created_date date
);
GO
create table vote(
vote_id int primary key identity(1,1),
maker_id int references [user]([user_id]) not null, 
lesson_id int references lesson(lesson_id) not null,
star int not null
);
GO
create table subject_statistic(
subject_statistic_id int primary key identity(1,1),
subject_id int references [subject]([subject_id]) not null, 
revenue float not null,
purchases int not null,
views int not null
);