3333333@ -0,0 +1,262 @@
<%-- 
    Document   : HomePage
    Created on : May 19, 2024, 11:43:11 PM
    Author     : admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" data-bs-theme="light">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css">
        <style>
            .center-text {
                display: flex;
                flex-direction: column;
                text-align: center;
                text-justify: distribute;
            }

            .course-list {
                justify-content: space-around;
            }

            .button-hower {
                color: #4D5E6F;
            }

            .button-hower:hover {
                background-color: white;
                width: 250px;
                transition: all 0.3s ease-in-out;
            }

            .course-list {
                background-color: #DBE0E9;
                border-radius: 4px;
            }

            .subject-list {
                display: flex;
                flex-direction: row;
                justify-content: space-between;
                flex-wrap: wrap;
                grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
                grid-gap: 20px;
            }

            .card-img {
                height: 215px;
            }

            .card-lesson {
                width: 300px;
            }

            .form-switch .form-check-input:focus {
                border-color: rgba(255, 255, 255, 0.192);
                box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);
                background-image: url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' viewBox='-4 -4 8 8'><circle r='3' fill='rgba(0,0,0,0.25)'/></svg>");
            }

            .form-switch .form-check-input:checked {
                background-color: #30D158;
                border-color: #30D158;
                background-image: url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' viewBox='-4 -4 8 8'><circle r='3' fill='rgba(255,255,255,1.0)'/></svg>");
            }
        </style>
    </head>

    <body>

        <nav class="navbar navbar-expand-lg bg-body-tertiary container">
            <div class="container-fluid">
                <a class="navbar-brand" href="./HomePage">StuQuiz</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="search navbar-collapse" id="navbarSupportedContent"
                     style="display: flex; flex-direction: row-reverse;">

                    <ul class="navbar-nav ml-auto mb-3 mb-lg-0" style="">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                               aria-expanded="false">
                                <c:if test="${sessionScope.userAuthorization==null}">
                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGU0uf9RH5npQE6jWFRwRHNWoTTyy4NuxPo0rzkJm8tA&s"
                                         alt="" style="height: 30px;width: 30px; border-radius: 50%;">
                                </c:if>
                                <c:if test="${sessionScope.userAuthorization!=null}">
                                    <img src="https://gcs.tripi.vn/public-tripi/tripi-feed/img/474074hYy/anh-nen-hoa-anh-dao-dep-nhat_025505349.jpg"
                                         alt="" style="height: 30px;width: 30px; border-radius: 50%;">
                                </c:if>
                            </a>
                            <ul class="dropdown-menu">
                                <c:if test="${sessionScope.userAuthorization!=null}">
                                    <li><a class="dropdown-item" href="#">View Profile</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                </c:if>
                                <c:if test="${sessionScope.userAuthorization!=null}">
                                    <li><a class="dropdown-item" href="./Logout">Logout</a></li>
                                    </c:if>
                                    <c:if test="${sessionScope.userAuthorization==null}">
                                    <li><a class="dropdown-item"
                                           href="./Account">Login/Register</a></li>
                                    </c:if>
                            </ul>
                        </li>
                    </ul>
                    <div class="form-check form-switch">
                        <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckChecked"
                               onchange="switchTheme()">
                    </div>
                </div>
            </div>
        </nav>
        <br><br><br><br><br>
        <h2 class="mx-auto center-text">LET'S GO TO FIND YOUR COURSE</h2>
        <div class="container text-center">
            <div class="row border course-list">
                <a class="col-md-2 btn button-hower">
                    All
                </a>
                <a class="col-md-2 btn button-hower">
                    Software technology
                </a>
                <a class="col-md-2 btn button-hower">
                    Data analysis
                </a>
                <a class="col-md-2 btn button-hower">
                    Logistic
                </a>
                <a class="col-md-2 btn button-hower">
                    Hotel management
                </a>
            </div>
            <br><br><br>
            <div class=" d-flex" style="width: 420px;">
                <form class="d-flex container" role="search" style="align-items: center;">
                    <input class="form-control me-2" type="search" placeholder="Find your lesson here" aria-label="Search">
                    <button class="btn btn-outline-success btn-sm" type="submit" style="margin-right: 20px;">Search</button>
                    <div class="form-check form-switch">
                        <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault">
                        <label for="flexSwitchCheckDefault"><span style="color: #4D5E6F;">Oldest</span></label>
                    </div>
                </form>
            </div>
        </div>

        <br>
        <div class="container subject-list">
            <div class="card card-lesson">
                <img src="https://titv.vn/wp-content/uploads/2023/07/lap-trinh-c-300-×-152-px.png"
                     class="card-img-top card-img" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's
                        content.</p>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Learning:</li>
                    <li class="list-group-item">Number of lessons:</li>
                </ul>
                <div class="card-body">
                    <a href="#" class="btn btn-outline-danger">Enroll Me</a>
                    <a href="#" class="btn btn-outline-info">More information</a>
                </div>
            </div>

            <div class="card card-lesson">
                <img src="https://titv.vn/wp-content/uploads/2023/01/Lap-trinh-JSP-2-300x225.png"
                     class="card-img-top card-img" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's
                        content.</p>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Learning:</li>
                    <li class="list-group-item">Number of lessons:</li>
                </ul>
                <div class="card-body">
                    <a href="#" class="btn btn-outline-danger">Enroll Me</a>
                    <a href="#" class="btn btn-outline-info">More information</a>
                </div>
            </div>

            <div class="card card-lesson">
                <img src="https://titv.vn/wp-content/uploads/2023/01/jdbc-300x225.png" class="card-img-top card-img"
                     alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's
                        content.</p>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Learning:</li>
                    <li class="list-group-item">Number of lessons:</li>
                </ul>
                <div class="card-body">
                    <a href="#" class="btn btn-outline-danger">Enroll Me</a>
                    <a href="#" class="btn btn-outline-info">More information</a>
                </div>
            </div>

            <div class="card card-lesson">
                <img src="https://titv.vn/wp-content/uploads/2023/01/Python-300x225.png" class="card-img-top card-img"
                     alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's
                        content.</p>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Learning:</li>
                    <li class="list-group-item">Number of lessons:</li>
                </ul>
                <div class="card-body">
                    <a href="#" class="btn btn-outline-danger">Enroll Me</a>
                    <a href="#" class="btn btn-outline-info">More information</a>
                </div>
            </div>

        </div>
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
<script>
                                   function switchTheme() {
                                       var checked = document.getElementById("flexSwitchCheckChecked").checked;
                                       if (checked) {
                                           document.documentElement.setAttribute("data-bs-theme", "light");
                                       } else {
                                           document.documentElement.setAttribute("data-bs-theme", "dark");
                                       }
                                   }
                                   const getPreferredTheme = () => {
                                       return window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light'
                                   }

                                   const setTheme = theme => {
                                       if (theme === 'auto' && window.matchMedia('(prefers-color-scheme: dark)').matches) {
                                           document.documentElement.setAttribute('data-bs-theme', 'dark')
                                       } else {
                                           document.documentElement.setAttribute('data-bs-theme', theme)
                                       }
                                   }
                                   setTheme(getPreferredTheme());
</script>
</html>