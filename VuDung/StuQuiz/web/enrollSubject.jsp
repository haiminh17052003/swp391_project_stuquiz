<%-- 
    Document   : enrollSubject
    Created on : Jun 19, 2024, 11:58:29 PM
    Author     : vtdle
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A==" crossorigin="anonymous" referrerpolicy="no-referrer">       
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Stuquiz - Enroll Subject</title>
        <style>
            .container {
                max-width: 40%;
                margin: 5%;
            }

            .card {
                border: 1px solid #ddd;
                border-radius: 8px;
                margin-bottom: 20px;
                overflow: hidden;
                background-color: #fff;
                box-shadow: 0 0 10px rgba(0,0,0,0.1);
            }

            .card-body {
                padding: 20px;
            }

            .card-code, .card-title {
                font-size: 18px;
                font-weight: bold;
                color: #333;
                margin-bottom: 10px;
                text-align: center;
            }

            .card-text {
                font-size: 16px;
                line-height: 1.6;
                margin-bottom: 15px;
                color: #666;
            }

            .card-img {
                max-width: 100%;
                height: auto;
                display: block;
                margin: 0 auto;
                border-radius: 8px 8px 0 0;
            }

            .btn-primary {
                background-color: #007bff;
                color: #fff;
                border: none;
                padding: 10px 20px;
                cursor: pointer;
                border-radius: 5px;
            }

            .btn-primary:hover {
                background-color: #0056b3;
            }

            .enroll-btn {
                background-color: #28a745;
                color: #fff;
                border: none;
                padding: 10px 20px;
                cursor: pointer;
                border-radius: 5px;
            }

            .enroll-btn:hover {
                background-color: #218838;
            }

            .title-enroll ,h1{
                text-align: center;
                color: red;
                margin: 5%;
            }

            .enroll-container{
                text-align: center;
                margin-bottom: 5%;
            }

            .card-img {
                border-top-left-radius: 8px;
                border-top-right-radius: 8px;
                height: 200px;
                object-fit: cover;
            }
        </style>
    </head>
    <body>
        <h1 class="title-enroll"><strong>CHOOSE YOUR PATH & LEARN TOGETHER!</strong></h1>
        <div class="container">   
            <c:forEach var="subjectEnrolledAndStatus" items="${subjects}">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-code">${subjectEnrolledAndStatus.subject.subjectCode}</h5>
                        <img src="${subjectEnrolledAndStatus.subject.subjectImage}" class="card-img-top card-img" alt="Subject-Img">
                        <div class="card-body">
                            <h5 class="card-title">${subjectEnrolledAndStatus.subject.subjectName}</h5>
                            <p class="card-text">${subjectEnrolledAndStatus.subject.subjectDescription}</p>
                            <p class="card-text">Price:
                                <c:choose>
                                    <c:when test="${subjectEnrolledAndStatus.salePrice != null && subjectEnrolledAndStatus.salePrice > 0}">
                                        <span style="text-decoration: line-through;">${subjectEnrolledAndStatus.subject.subjectPrice} VND</span>                               
                                        <span style="color: red;">
                                            <c:out value="${subjectEnrolledAndStatus.subject.subjectPrice * subjectEnrolledAndStatus.salePrice / 100}"/> VND                                           
                                        </span>
                                        <span style="color: green;">(${100 - subjectEnrolledAndStatus.salePrice}%) off</span>
                                    </c:when>
                                    <c:otherwise>
                                        <span style="color: red;">${subjectEnrolledAndStatus.subject.subjectPrice} VND</span>
                                    </c:otherwise>
                                </c:choose>
                            </p>                         
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
        <div class="enroll-container">
            <c:forEach var="subjectEnrolledAndStatus" items="${subjects}">
                <form id="enrollForm${subjectEnrolledAndStatus.subject.subjectId}" action="ViewEnrollSubject" method="post">
                    <input type="hidden" name="subjectId" value="${subjectEnrolledAndStatus.subject.subjectId}">
                    <input type="hidden" name="subjectName" value="${subjectEnrolledAndStatus.subject.subjectName}">                        
                    <button type="submit" class="btn btn-primary enroll-btn">Enroll</button>
                </form>
            </c:forEach>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    </body>
</html>