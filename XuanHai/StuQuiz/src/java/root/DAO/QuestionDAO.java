/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package root.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import root.entities.main.Course;
import root.entities.main.Question;
import root.entities.main.User;
import root.jdbc.RowMapper;
import root.jdbc.SQLServerConnection;

/**
 *
 * @author admin
 */
public class QuestionDAO implements RowMapper<Question> {

    @Override
    public Question mapRow(ResultSet rs) throws SQLException {
        return Question.builder()
                .questionId(rs.getLong("question_id"))
                .subjectId(rs.getLong("subject_id"))
                .questionContent(rs.getString("question_content"))
                .questionImage(rs.getString("question_image"))
                .questionLevel(rs.getLong("question_level"))
                .isActive(rs.getBoolean("is_active"))
                .build();
    }

    @Override
    public boolean addNew(Question t) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Question> getAll() throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Question getById(String id) throws SQLException, ClassNotFoundException {
        Question question = null;
        String sql = """
              select * from [question] where [question_id]=?
              """;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, parseLong(id));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                question = mapRow(rs);
            }
            if (question == null) {
                question = Question.builder().questionId(0L).build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return question;
    }

    @Override
    public boolean updateById(String id, Question t) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean deleteById(String id) throws SQLException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<Question> getListQuestionByQuizId(String quizId) throws SQLException, ClassNotFoundException {
        List<Question> list = new ArrayList<>();
        String sql = """
                     SELECT q.*
                     FROM question q
                     JOIN quiz_bank qb ON q.question_id = qb.question_id
                     WHERE qb.quiz_id = ?;
                     """;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, parseLong(quizId));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(mapRow(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Question> getListQuestionBySubjectId(String id) throws SQLException, ClassNotFoundException {
        List<Question> list = new ArrayList<>();
        String sql = """
                    SELECT * FROM question WHERE subject_id = ?
                     """;
        try (Connection con = SQLServerConnection.getConnection(); PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setObject(1, parseLong(id));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(mapRow(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        List<Question> q = new ArrayList<>();
        QuestionDAO dao = new QuestionDAO();
        q = dao.getListQuestionBySubjectId(1 + "");
        System.out.println(q);
    }

}
