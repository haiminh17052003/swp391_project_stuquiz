<%-- 
    Document   : LessonDetail
    Created on : May 20, 2024, 5:03:24 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                background-color: #f4f4f4;
            }

            .navbar {
                display: flex;
                justify-content: space-between;
                align-items: center;
                background-color: #333;
                padding: 20px 28px;
            }

            .nav-left {
                display: flex;
                align-items: center;
                margin-left: 10%;
            }

            .nav-right {
                display: flex;
                align-items: center;
                margin-right: 10%;
            }

            .nav-center {
                position: absolute;
                left: 50%;
                transform: translateX(-50%);
            }

            .navbar a {
                color: white;
                text-decoration: none;
                padding: 0 15px;
            }

            .brand-name {
                font-size: 40px;
                font-weight: bold;
            }

            .dropdown {
                position: relative;
                display: inline-block;
            }

            .dropbtn {
                background-color: #333;
                color: white;
                border: none;
                padding: 14px 16px;
                font-size: 16px;
                cursor: pointer;
            }

            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
                z-index: 1;
            }

            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }

            .dropdown-content a:hover {
                background-color: #f1f1f1;
            }

            .dropdown:hover .dropdown-content {
                display: block;
            }

            .dropdown:hover .dropbtn {
                background-color: #555;
            }

            .lesson-details {
                background-color: gray;
                height: 80px;
                display: flex;
                justify-content: left;
                align-items: center;
            }

            .lesson-details h1 {
                color: white;
                margin: 0;
                font-size: 24px;
                margin-left: 10%;
            }

            .lesson-details a {
                text-decoration: none;
                color: black;
            }

            .form-container {
                background-color: #fff;
                padding: 10px 50px;
                border: 1px solid #ddd;
                border-radius: 5px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }

            .form-container label {
                font-size: 18px;
                margin: 10px 10px;
                display: flex;
                flex-direction: row;
                justify-content: space-between;
                width: 100%;
            }

            .form-group label {
                font-weight: bold;
            }

            .form-control {
                border-radius: 15px;
                border-color: #ced4da;
                padding: 10px;
            }

            table {
                width: 100%;
                border-collapse: collapse;
                margin-bottom: 15px;
            }

            table,
            th,
            td {
                border: 1px solid #ddd;
            }

            th,
            td {
                padding: 10px;
                text-align: left;
            }

            td input[type="text"],
            td input[type="number"] {
                width: 100%;
                padding: 8px;
                box-sizing: border-box;
                border: none;
                margin: 0;
            }

            .form-actions {
                text-align: right;
            }

            .form-actions button {
                padding: 10px 20px;
                border: none;
                border-radius: 3px;
                background-color: #5cb85c;
                color: #fff;
                cursor: pointer;
            }

            .form-actions button[type="button"] {
                background-color: #d9534f;
            }

            .form-actions button+button {
                margin-left: 10px;
            }
        </style>
    </head>

    <body>
        <nav class="navbar">
            <div class="nav-left">
                <div class="dropdown">
                    <button class="dropbtn">Services</button>
                    <div class="dropdown-content">
                        <a href="#">Service 1</a>
                        <a href="#">Service 2</a>
                        <a href="#">Service 3</a>
                    </div>
                </div>
                <a href="#" class="contact-link">Contact</a>
            </div>
            <div class="nav-center">
                <a href="#" class="brand-name">StuQuiz</a>
            </div>
            <div class="nav-right">
                <a href="#" class="login-signup"><i class="fas fa-user"></i> Login/Sign Up</a>
            </div>
        </nav>

        <div class="lesson-details">
            <h1><a href="#" class="fas fa-arrow-left"></a> Lesson Details</h1>
        </div>

        <div class="container form-group form-container mt-5 col">
            <form action="LessonDetail" method="post">
                <input type="text" name="maxL" id="maxL" value="1" hidden="">
                <label for="lessons">Add Lesson</label>
                <div class="form-group">
                    <table>
                        <thead>
                            <tr>
                                <th>Lesson Name</th>
                                <th>Video Link</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="text" name="lessonName1"></td>
                                <td><input type="text" name="videoLink1"></td>
                                <td><input type="text" name="lessonDescription1" readonly onclick="openDescriptionModal(this)"></td>
                                <td><button type="button" onclick="removeRow(this)">Remove</button></td>
                            </tr>
                        </tbody>
                    </table>
                    <button type="button" onclick="addRow()">Add Lesson</button>
                </div>
                <div class="form-actions">
                    <button type="button">Back</button>
                    <button type="submit" name="actionCheck" value="addLessons">Submit</button>
                </div>
            </form>
        </div>

        <!-- Description Modal -->
        <div class="modal fade" id="descriptionModal" tabindex="-1" aria-labelledby="descriptionModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="descriptionModalLabel">Add Description</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <textarea class="form-control" id="modalDescription" rows="5"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="saveDescription()">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            let currentDescriptionInput;
            var i = 1;
            function addRow() {
                i++;
                document.getElementById("maxL").value = i;
                const table = document.querySelector('table tbody');
                const rowCount = table.rows.length;
                console.log(rowCount);
                const row = table.insertRow();

                const cell1 = row.insertCell(0);
                const cell2 = row.insertCell(1);
                const cell3 = row.insertCell(2);
                const cell4 = row.insertCell(3);

                cell1.innerHTML = '<input type="text" name="lessonName'+i+'">';
                cell2.innerHTML = '<input type="text" name="videoLink'+i+'">';
                cell3.innerHTML = '<input type="text" name="lessonDescription'+i+'" readonly onclick="openDescriptionModal(this)">';
                cell4.innerHTML = `<button type="button" onclick="removeRow(this)">Remove</button>`;
            }

            function removeRow(button) {
                const row = button.parentNode.parentNode;
                row.parentNode.removeChild(row)
                i--;
            }

            function openDescriptionModal(input) {
                currentDescriptionInput = input;
                const modal = new bootstrap.Modal(document.getElementById('descriptionModal'));
                document.getElementById('modalDescription').value = currentDescriptionInput.value;
                modal.show();
            }

            function saveDescription() {
                const modal = bootstrap.Modal.getInstance(document.getElementById('descriptionModal'));
                currentDescriptionInput.value = document.getElementById('modalDescription').value;
                modal.hide();
            }
        </script>

        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js"></script>
    </body>
</html>
